package br.edu.up.principal;

import java.util.Scanner;

public class Programa {
	
	public static void main(String[] args) {
		
		
		// Criação de um objeto chamado leitor.
		// Uso da palavra (new).
		Scanner leitor = new Scanner(System.in);
		
		// Usar o método nextInt()...
//		System.out.println("Informe o número 1:");
//		int numero1 = leitor.nextInt();
		
//		System.out.println("Informe o número 2:");
//		int numero2 = leitor.nextInt();
		
        // PROGRAMAÇÃO FUNCIONAL 
		// Uso de biblioteca para realizar as funções...
//		double resultado = Biblioteca.somar(numero1, numero2);
//		System.out.println("O resultado da soma é: " + resultado);
//		resultado = Biblioteca.subtrair(numero1, numero2);
//		System.out.println("O resultado da subtração é: " + resultado);
//		resultado = Biblioteca.multiplicar(numero1, numero2);
//		System.out.println("O resultado da multiplicação é: " + resultado);
//		resultado = Biblioteca.dividir(numero1, numero2);
//		System.out.println("O resultado da dividir é: " + resultado);
		
		// PROGRAMAÇÃO ORIENTADA A OBJETOS
		// Uso do objeto calculadora para realizar as operações...		
//		Calculadora calc  = new Calculadora();
//		double resultado = calc.somar(numero1, numero2);
//		System.out.println("O resultado da soma é: " + resultado);
//		resultado = calc.subtrair(numero1, numero2);
//		System.out.println("O resultado da subtração é: " + resultado);
//		resultado = calc.multiplicar(numero1, numero2);
//		System.out.println("O resultado da multiplicação é: " + resultado);
//		resultado = calc.dividir(numero1, numero2);
//		System.out.println("O resultado da dividir é: " + resultado);
		
		
		//Início do menu...
		String opcao = "";
		
		do {
			System.out.println("Escolha a opção:");
			System.out.println("A (a):");
			System.out.println("B (b):");
			System.out.println("Voltar (v):");
			System.out.println("Sair (s):");
			opcao = leitor.nextLine();
			
			if (opcao.equals("v")) {
				System.out.println("Vai voltar para no nível anterior...");
			}
			
		} while(!opcao.equals("s"));
		
		
						
		// Uso do méto close();
		leitor.close();
	}
	
	
	
}
